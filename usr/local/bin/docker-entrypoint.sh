#!/bin/bash

set -e

if [ "$1" = 'pdns_server' ]; then
    exec /usr/local/sbin/pdns_server
fi

exec "$@"