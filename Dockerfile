#########################
# Create base container #
#########################
FROM ubuntu:24.04 as base
LABEL maintainer="pidydx"

# Base setup
ENV APP_USER=pdns
ENV APP_GROUP=pdns

ENV PDNS_AUTH_VERSION=4.9.0

ENV BASE_PKGS libkrb5-3 libldap2 libluajit-5.1-2 libsodium23 lua5.4

# Update users and groups
RUN userdel ubuntu
RUN groupadd -g 1000 ${APP_GROUP} \
 && useradd -M -N -u 1000 ${APP_USER} -g ${APP_GROUP} -s /usr/sbin/nologin

# Update and install base packages
RUN apt-get update -q \
 && DEBIAN_FRONTEND=noninteractive apt-get upgrade -yq \
 && DEBIAN_FRONTEND=noninteractive apt-get install -yq --no-install-recommends ca-certificates \
 && apt-get update -q \
 && DEBIAN_FRONTEND=noninteractive apt-get install -yq --no-install-recommends ${BASE_PKGS} \
 && rm -rf /var/lib/apt/lists/*


##########################
# Create build container #
##########################
FROM base AS builder

# Set build dependencies
ENV BUILD_DEPS autoconf automake bison build-essential git flex gawk libboost-all-dev libcurl4 libcurl4-openssl-dev \
               libkrb5-dev libldap2-dev libluajit-5.1-dev libsodium-dev libsqlite3-dev libssl-dev libtolua-dev libtool \
               libtool libyaml-cpp-dev libyaml-cpp-dev lua-yaml-dev luajit pkg-config ragel wget

# Install build dependencies
RUN apt-get update -q \
 && DEBIAN_FRONTEND=noninteractive apt-get install -yq --no-install-recommends ${BUILD_DEPS}

# Run build
WORKDIR /usr/src
RUN wget -nv https://downloads.powerdns.com/releases/pdns-${PDNS_AUTH_VERSION}.tar.bz2
RUN tar xf pdns-${PDNS_AUTH_VERSION}.tar.bz2
WORKDIR /usr/src/pdns-${PDNS_AUTH_VERSION}
RUN ./configure --sysconfdir=/etc/pdns --mandir=/usr/share/man --disable-lua-records --enable-libsodium --with-modules='ldap'
RUN make
RUN make install-strip


##########################
# Create final container #
##########################
FROM base

# Prepare container
COPY etc/ /etc/
COPY --from=builder /usr/local /usr/local/
COPY usr/ /usr/

RUN mkdir -p /run/pdns \
 && chown ${APP_USER}:${APP_GROUP} /run/pdns


EXPOSE 5353/tcp 5353/udp
VOLUME ["/etc/pdns"]

USER $APP_USER
ENTRYPOINT ["docker-entrypoint.sh"]
CMD ["pdns_server"]
